
document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        const map = initMap()
        addBicisToMap(map)
    }

};

function initMap() {
    const accessToken = 'pk.eyJ1IjoicGxpemFyZG8iLCJhIjoiY2tpZ21nZ2RuMGxwMzJyczFzYWtnaHRmNSJ9.hvWeEHSYgCBhYZi9QYYqbg'
    const center = [-53.783424377598685, -67.7139908567125]
    var map = L.map('map', {
        center: center,
        zoom: 13
    });
    L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${accessToken}`, {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: accessToken
    }).addTo(map);
    return map
}
const addBicisToMap = async (map) => {
    const res = await fetch('http://localhost:3000/api/bicicletas')
    const json = await res.json()
    // bicis = [
    //     [-53.77074170104641, -67.71146835717101],
    //     [-53.770091065497766, -67.70716974221541],
    //     [-53.77244569892988, -67.70701247581354],
    //     [-53.76491660437291, -67.73170330090797],
    //     [-53.77281747112721, -67.73369534242251],
    // ]
    console.log(json.bicicletas)
    json.bicicletas.forEach( bici => {
        L.marker(bici.ubicacion,{ title: bici.id }).addTo(map);
    })
}