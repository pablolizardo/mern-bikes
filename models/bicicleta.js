var Bicicleta = function(id,color,modelo,ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return `id ${this.id} modelo: ${this.modelo} color ${this.color}`;
}

Bicicleta.allBicis = []

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find( x=> x.id == aBiciId)
    if (aBici ) {
        return aBici;

    } else {
        throw new Error(`No existe bicicleta con el id ${aBiciId}`)
    }
}

Bicicleta.removeById = function(aBiciId) {
    // var aBici = Bicicleta.findById(aBiciId)
    for(var i = 0; i < Bicicleta.allBicis.length; i ++) {
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1)
            break;
        }
    }
}

let a = new Bicicleta(1,'rojo', 'urbana', [-53.77074170104641, -67.71146835717101])
let b = new Bicicleta(2,'verde', 'rural', [-53.770091065497766, -67.70716974221541])
let c = new Bicicleta(3,'rojo', 'urbana', [-53.77244569892988, -67.70701247581354])
let d = new Bicicleta(4,'blanca', 'de ruta', [-53.76491660437291, -67.73170330090797])
let e = new Bicicleta(5,'blanca', 'urbana', [-53.77281747112721, -67.73369534242251])

// Bicicleta.add(a)
// Bicicleta.add(b)
// Bicicleta.add(c)
// Bicicleta.add(d)
// Bicicleta.add(e)

module.exports = Bicicleta