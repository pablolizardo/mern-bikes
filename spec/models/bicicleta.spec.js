const Bicicleta = require('./../../models/bicicleta')

describe('test list bikes', () => {

    it('array vacio ', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
});

describe('agregar bikes', () => {

    it('agregar ', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0)
        let bici = new Bicicleta(10,'black','urben',[-31,-50])
        Bicicleta.add(bici)
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(bici)

    })
});
